class RPNCalculator
  operators = [:+, :-, :*, :/]

  def initialize
    @stack = []
  end

  def operation?(ch)
    [:+, :-, :*, :/].include?(ch.to_sym)
  end
  def do_operation(op_symbol)
    raise "calculator is empty" if @stack.count < 2
    second_op = @stack.pop
    first_op = @stack.pop

    case op_symbol
    when :+
      @stack << first_op + second_op
    when :-
      @stack << first_op - second_op
    when :*
      @stack << first_op * second_op
    when :/
      @stack << first_op.fdiv(second_op)
    else
      @stack << first_op << second_op
      raise "No such operation: #{op_symbol}"
    end
  end

  def push(num)
    @stack << num
  end

  def value
    @stack.last
  end

  def plus
    do_operation(:+)
  end

  def minus
    do_operation(:-)
  end

  def times
    do_operation(:*)
  end

  def divide
    do_operation(:/)
  end

  def tokens(string)
    tokens = string.split
    tokens.map { |ch| operation?(ch) ? ch.to_sym : ch.to_i }
  end

  def evaluate(string)
    tokens = tokens(string)
    tokens.each do |token|
      case token
      when Integer
        token.to_i
        push(token)
      else
        do_operation(token)
      end
    end

    self.value
  end

end
